class ValidatorController < ApplicationController
  def index
  end

  def robots
    puts params
    list = Validator.parse_urls_from_list(params[:file].tempfile.read)
    results = []
    list.each do |item|
       results << Validator.check_site(item, params[:agent]) unless item.empty?
    end
    render json: results.to_json

  end

  def parse_url
    render json: Validator.check_site(params[:url], params[:agent])
  end
end
