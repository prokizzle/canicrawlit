require "addressable/uri"


class Validator
  def self.check_site(site, agent)
    {url: site, allowed: all_robots_ok?(site, agent)}.to_json
  end

  def self.all_robots_ok?(site, custom_agent)
    if site.length > 3
      custom = Robotex.new custom_agent
      all = Robotex.new "*"
      custom.allowed?(site) && all.allowed?(site)
    end
  end

  def self.valid_url(url)
    uri = Addressable::URI.parse(url)
    if %w( http https ).include?(uri.scheme)
      return url.split("\n").first
    elsif url.length > 2
      return "http://#{url.split("\n").first}"
    else
      return ""
    end
  end

  def self.parse_urls_from_list(urls)
    list = urls.split("\n", -1).map do |x|
      self.valid_url(x.gsub("\r",""))
    end
    return list
  end
end
