jQuery ->

  $('#saveCustomAgent').on "click", ->
    $("#drop-area-div").dmUploader
      url: "/process"
      allowedTypes: "text/plain"
      maxFileSize: 41943040
      extraData:
        agent: $('#customAgentField').val()

      onUploadSuccess: (id, data) ->
        console.log "success"
        $('#status').html("Processing...")
        $('#allowed').show()
        $('#disallowed').show()
        $.each data, (index, value) ->
          result = $.parseJSON(value)
          if result.allowed is true
            $("#allowed ul").append("<li>#{result.url}</li>")
          else
            $("#disallowed ul").append("<li>#{result.url}</li>")
          return
        $('#uploadArea').hide()
        $('#resultsArea').show()
        $('#startOver').show()
        return

      onNewFile: (id) ->
        console.log "Starting to upload #" + id
        console.log
        $('#status').html("Uploading...")
        return

      onUploadProgress: (id, percent) ->
        console.log "Upload of #" + id + " is at %" + percent
        $('#status').html("Processing...") if percent is 100
        return

      onBeforeUpload: (id) ->
        console.log "Starting to upload #" + id
        return

      onFileSizeError: (file) ->
        console.log "File size of " + file.name + " exceeds the limit"
        $('#status').html("Too big")
        # Alert.error "File size may not exceed 40MB"
        return

      onUploadError: (id, message) ->
        console.log "Error trying to upload file"
        $('#status').html("Error")
        return
    $('#chooseAgent').hide()
    $('#uploadArea').show()
    return false
