$(document).ready(function() {
    $('#processRobots').on("click", function() {
        $('.spinner').show();
        $('#allowed').show();
        $('#disallowed').show();
        var lines = $('#urllist').val().replace(/\r\n/g, "\n").split("\n");
        _.each(lines, function(line) {
            evaluateUrl(line);
        });
        $('#uploadArea').fadeOut(1000);
        $('#resultsArea').fadeIn(1000);
        $('#startOver').fadeIn(1000);
        $('.spinner').hide();
    });
});


var evaluateUrl = function(url) {
  $('.spinner').show();
    $.getJSON('/check', {
        agent: $('#customAgentField').val(),
        url: url
    }, function(result) {
        if (!_.isEmpty(result.url)) {
            if (result.allowed) {
                $("#allowed p").append(result.url + "<br/>");
            } else {
                $("#disallowed p").append(result.url + "<br/>");
            }
            console.log(result);
        }

    });
    $('.spinner').hide();
}

var showSpinner = function() {
    $('.spinner').each(function() {
        $(this).show()
    });
}

var hideSpinner = function() {
    $('.spinner').each(function() {
        $(this).hide()
    });
}
